package acernis.info212.v2016.uib.no.adapters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;

import acernis.info212.v2016.uib.no.beans.Schedule;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

/**
 * Created by QRT on 20.11.2016.
 */


public class ScheduleDeserializer implements JsonDeserializer<Schedule> {

    public Schedule deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {

        if (json == null)
            return null;

        JsonObject jo = json.getAsJsonObject();

        int ID = jo.get("ID").getAsInt();
        int userID = jo.get("userID").getAsInt();
        int TSID = jo.get("TSID").getAsInt();

        Date workDate = null;
        try {
            workDate = new Date(ToolsForActivities.getJsonDateFormatter().parse(jo.get("workDate").getAsString()).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Time startWorkTime = null;
        try {
            startWorkTime = new Time(ToolsForActivities.getJsonTimeFormatter().parse(jo.get("startWorkTime").getAsString().trim()).getTime());
           // System.out.println("StartWork  " + startWorkTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Time endWorkTime = null;
        try {
            endWorkTime = new Time(ToolsForActivities.getJsonTimeFormatter().parse(jo.get("endWorkTime").getAsString().trim()).getTime());
           // System.out.println("EndWork  " + endWorkTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Time checkIN = null;
        try {
            checkIN = new Time(ToolsForActivities.getJsonTimeFormatter().parse(jo.get("checkIN").getAsString().trim()).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Time checkOUT = null;
        try {
            checkOUT = new Time(ToolsForActivities.getJsonTimeFormatter().parse(jo.get("checkOUT").getAsString().trim()).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return new Schedule(ID, userID, TSID, workDate,
                startWorkTime,endWorkTime,checkIN,checkOUT);

    }
}