package acernis.info212.v2016.uib.no.activities;

import android.app.Application;

/**
 * Created by stian on 16.11.2016.
 */

public class App extends Application {
    private static String token;
    private static App instance;
    public static App getInstance() { return instance; }
    public static String getToken(){return token;}
    public static void setToken(String stoken){token = stoken;}
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}