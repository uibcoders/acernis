package acernis.info212.v2016.uib.no.beans;

import org.apache.commons.net.time.TimeTCPClient;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class GetTime {

    public static void main(String[] args) {
        GetTime gt = new GetTime();
        System.out.println(gt.reformatDate());
        System.out.println(gt.getDate());
    }

    public Date getDate() {
        try {
            TimeTCPClient client = new TimeTCPClient();
            try {

                client.setDefaultTimeout(5000);

                client.connect("198.111.152.100");

                return client.getDate();
            } finally {
                client.disconnect();

            }
        } catch (IOException e) {
            e.printStackTrace();

        }


        return null;
    }

    /**
     * Returns the time reformated in y,m,d, h,m,s timezone
     * @return
     */
    public String reformatDate() {
        Date date = new Date(getDate().getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        sdf.setTimeZone(TimeZone.getTimeZone("CEST")); // give a timezone reference for formatting (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

}