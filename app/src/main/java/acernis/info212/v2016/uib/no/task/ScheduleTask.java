package acernis.info212.v2016.uib.no.task;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import acernis.info212.v2016.uib.no.activities.App;
import acernis.info212.v2016.uib.no.activities.MainActivity;
import acernis.info212.v2016.uib.no.adapters.ScheduleDeserializer;
import acernis.info212.v2016.uib.no.beans.Schedule;
import acernis.info212.v2016.uib.no.tools.HTTPConnection;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

/**
 * Created by stian on 16.11.2016.
 */

public class ScheduleTask extends AsyncTask<Object, Object, List<Schedule>> {
    private HashMap<String, String> connParams = new HashMap<>();
    private HTTPConnection connection;
    private MainActivity.StartFragment scheduleActivity;
    private boolean today = false;

    public ScheduleTask() {
        this.connParams.put("type", "schedule");
    }

    public ScheduleTask(String from, String to, boolean today) {
        this.today = today;
        this.connParams.put("type", "schedule");
        this.connParams.put("from", from.trim());
        this.connParams.put("to", to.trim());
    }

    /**
     * Parses the text JSON from the server and creates a map from the input.
     * @param jsonString - the JSON data map
     * @return
     */
    public Map<String, Schedule> parseSchedule(String jsonString) {
        Map<String, Schedule> sMap = null;
        JsonObject jobject = null;
        try {
            JsonElement jelement = new JsonParser().parse(jsonString);
            jobject = jelement.getAsJsonObject();

            String scheduleString = jobject.getAsJsonObject("schedule").toString();
            System.out.println("ScheduleString = " + scheduleString);
            GsonBuilder gb = new GsonBuilder();
            gb.registerTypeAdapter(Schedule.class, new ScheduleDeserializer());

            Gson g = gb.create();
            Type stringStringMap = new TypeToken<Map<String, Schedule>>() {
            }.getType();
            sMap = g.fromJson(scheduleString, stringStringMap);

        } catch (Exception e) {

            System.out.println(e.getMessage() + "Error parsing json to map --> " + jsonString);

        }
        return sMap;
    }

    /**
     * Gets JSON hashmap from server and converts it to a usable list
     * @param jsonString - JSON from server
     * @return
     */
    public List<Schedule> getSchedule(String jsonString) {
        List<Schedule> scheduleList = new ArrayList<>();
        if (jsonString == null) {
            return null;
        }
        try {
            Map<String, Schedule> temp = parseSchedule(jsonString);
            if (temp == null) {
                return null;
            }


            for (Schedule sc : temp.values()
                    ) {

                // Schedule schedule = new Gson().fromJson(sc, Schedule.class);
                scheduleList.add(sc);
            }


        } catch (JsonParseException e) {
            e.printStackTrace();
        }

        return scheduleList;
    }

    /**
     * Runs in background, getting the schedule from server by JSON hashmap.
     * @param params
     * @return
     */
    @Override
    protected List<Schedule> doInBackground(Object[] params) {
        System.out.println("Getting SCHEDULE " + App.getToken());

        if (connParams == null) {
            System.out.println("No params set, initialize!");
            return null;
        }
        connection = HTTPConnection.getInstance();
        return getSchedule(connection.getJson(connParams, App.getToken()));
    }

    /**
     * Runs when taskm is done.
     * Updates listviews with data from server.
     * @param schedule
     */
    protected void onPostExecute(List<Schedule> schedule) {
        if (schedule == null) return;
        Collections.sort(schedule);
        for (Schedule sc : schedule
                ) {
            System.out.println(ToolsForActivities.isToday(sc.workDate) + "  " + sc.toString());
            if (!ToolsForActivities.isToday(sc.workDate) ) {
                getScheduleActivity().addWeek(sc);
            } else {
                getScheduleActivity().addDay(sc);
            }

        }


    }


    public void setScheduleActivity(MainActivity.StartFragment scheduleActivity) {
        this.scheduleActivity = scheduleActivity;
    }

    public MainActivity.StartFragment getScheduleActivity() {
        return this.scheduleActivity;
    }

    public HashMap<String, String> getConnParams() {
        return connParams;
    }

    public void setConnParams(HashMap<String, String> connParams) {
        this.connParams = connParams;
    }

    public HTTPConnection getConnection() {
        return connection;
    }

    public void setConnection(HTTPConnection connection) {
        this.connection = connection;
    }


}
