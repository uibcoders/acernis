package acernis.info212.v2016.uib.no.tools;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Cisse on 01.10.2016.
 */

public class ToolsForActivities {

    private static SimpleDateFormat swf = new SimpleDateFormat("EEEE");
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat jsonDateFormat = new SimpleDateFormat("MMM dd, yyyy");
    private static SimpleDateFormat jsonTimeFormat = new SimpleDateFormat("hh:mm:ss aa", Locale.US);

    private static SimpleDateFormat stf = new SimpleDateFormat("HH:mm");
    private static SimpleDateFormat sdtf = new SimpleDateFormat("HH:mm yyyy-MM-dd ");


    public static SimpleDateFormat getDateFormatter() {
        return sdf;
    }

    public static SimpleDateFormat getTimeFormatter() {
        return stf;
    }
    public static SimpleDateFormat getWeekNameFormatter() {
        return swf;
    }
    public static SimpleDateFormat getDateTimeFormatter() {
        return sdtf;
    }
    public static SimpleDateFormat getJsonDateFormatter() {
        return jsonDateFormat;
    }
    public static SimpleDateFormat getJsonTimeFormatter() {
        return jsonTimeFormat;
    }




    public static void toast(final Context context, final String message) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public static String parseJsonToken(String token) {
        System.out.println(token);
        String result = null;
        try {
            JSONObject tempObject = new JSONObject(token);
            String parsedToken = tempObject.getString("token");
            String parsedType = tempObject.getString("type");

            if (parsedToken.length() > 0 && parsedType.length() > 0) {
                result = parsedType + " " + parsedToken;
                return result;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }


    public static boolean isToday(Date date) {
        return (Calendar.getInstance().getTime().compareTo(date) != -1);
    }


}
