package acernis.info212.v2016.uib.no.task;

import android.app.Activity;
import android.os.AsyncTask;

import java.util.HashMap;

import acernis.info212.v2016.uib.no.activities.App;
import acernis.info212.v2016.uib.no.tools.HTTPConnection;

/**
 * Created by stian on 16.11.2016.
 */

public class CheckIOTask extends AsyncTask<Object, Object, Boolean> {
    private HashMap<String, String> connParams = new HashMap<>();
    private Activity mainActivity; //The android main activity
    private HTTPConnection connection;  //The connection to the server

    /**
     *
     */
    public CheckIOTask(String type) {
        connParams.put("type", type);

    }


    /**
     * Runs in background when executed from main thread
     *
     * @param params
     * @return
     */
    @Override
    protected Boolean doInBackground(Object[] params) {
        System.out.println("Trying to register check in/out ");
        if (connParams == null) {
            System.out.println("No params set, initialize!");
            return false;
        }
        connection = HTTPConnection.getInstance();
        System.out.println("JSON data from check in/out: " + connection.getJson(connParams, App.getToken()));
        return false;
    }

    /**
     * Runs when background task has finished.
     * Updates main gui.
     */
    protected void onPostExecute(boolean success) {
        System.out.println("Check in/out Success  = " + success);

    }


    public HashMap<String, String> getConnParams() {
        return connParams;
    }

    public void setConnParams(HashMap<String, String> connParams) {
        this.connParams = connParams;
    }

    public HTTPConnection getConnection() {
        return connection;
    }

    public void setConnection(HTTPConnection connection) {
        this.connection = connection;
    }
}


