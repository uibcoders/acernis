package acernis.info212.v2016.uib.no.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import acernis.info212.v2016.uib.no.task.LoginTask;
import acernis.info212.v2016.uib.no.task.R;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

/**
 * Created by Cisse on 29.09.2016.
 */

public class LoginActivity extends AppCompatActivity {

    private ToolsForActivities tools;
    private Context context;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private RelativeLayout relLay;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }


        context = this;
        relLay = (RelativeLayout) findViewById(R.id.relativeLay);

        loginOrRegister();

    }

    //login - register text
    public void loginOrRegister() {

        final EditText userName = (EditText) findViewById(R.id.usernameTField);
        final EditText password = (EditText) findViewById(R.id.passwordTField);
        Button login = (Button) findViewById(R.id.loginButton);
       // Button register = (Button) findViewById(R.id.registerButton);

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String name = userName.getText().toString().toLowerCase().trim();
                String pass = password.getText().toString().toLowerCase().trim();


                if (name.isEmpty() || pass.isEmpty()) {
                    tools.toast(context, "Please fill in username and password!");

                } else {
                    tools.toast(context, "Loggin in....");
                    LoginTask loginTask = new LoginTask(name, pass);
                    loginTask.setLoginActivity(LoginActivity.this);
                    loginTask.execute();

                    //Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                    //LoginActivity.this.startActivity(mainIntent);
                }
            }
        });

        /*register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.redButton) {
            this.relLay.setBackgroundColor(Color.RED);
            return true;
        }
        if (id == R.id.greenButton) {
            this.relLay.setBackgroundColor(Color.GREEN);
            return true;
        }
        if (id == R.id.blueButton) {
            this.relLay.setBackgroundColor(Color.BLUE);
            return true;
        }
        if (id == R.id.yellowButton) {
            this.relLay.setBackgroundColor(Color.YELLOW);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
}
