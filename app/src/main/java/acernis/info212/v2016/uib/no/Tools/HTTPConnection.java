package acernis.info212.v2016.uib.no.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import acernis.info212.v2016.uib.no.activities.App;
import acernis.info212.v2016.uib.no.task.R;

/**
 * Created by cisse on 30.09.2016.
 */
public class HTTPConnection {

    private URL DB_RETRIEVAL = null, DB_LOGIN = null;

    public HTTPConnection() {
        try {
            DB_RETRIEVAL = new URL(App.getInstance().getString(R.string.URL_DATABASE));
            DB_LOGIN = new URL(App.getInstance().getString(R.string.URL_LOGIN));
        } catch (MalformedURLException e) {
            e.printStackTrace();

        }
    }

    private static HTTPConnection instance;

    public static HTTPConnection getInstance() {
        if (instance == null) instance = new HTTPConnection();
        return instance;
    }

    public String getJson(HashMap<String, String> postParams, String authorization) {

        HttpURLConnection client = null;
        String data = "";

        BufferedReader reader = null;
        try {
            URL url;
            if (authorization != null) {
                url = DB_RETRIEVAL;
            } else {
                url = DB_LOGIN;
            }
            client = (HttpURLConnection) url.openConnection();
            client.setConnectTimeout(5000);
            client.setReadTimeout(5000);
            client.setDoOutput(true);
            client.setRequestMethod("POST");
            client.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            if (authorization != null) {
                client.setRequestProperty("Authorization", authorization);
            }

            client.connect();

            OutputStream os = client.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            String p = getPostDataString(postParams);
            writer.write(p);

            System.out.println("URL : " + url.toString() + "?" + p);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = client.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    data += line;
                }
            } else {
                data = null;
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.disconnect();
                System.out.println("CONNECTION CLOSED");
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        System.out.println("DATA : " + data);
        return data;
    }


    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }


}



