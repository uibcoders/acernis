package acernis.info212.v2016.uib.no.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import acernis.info212.v2016.uib.no.adapters.ScheduleAdapter;
import acernis.info212.v2016.uib.no.beans.GetTime;
import acernis.info212.v2016.uib.no.beans.Schedule;
import acernis.info212.v2016.uib.no.task.CheckIOTask;
import acernis.info212.v2016.uib.no.task.R;
import acernis.info212.v2016.uib.no.task.ScheduleTask;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static StartFragment startFragment;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class StartFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static ScheduleAdapter weekAdapter;
        private static ScheduleAdapter dayAdapter;
        private static ArrayList<Schedule> listItems;
        private static ArrayList<Schedule> listItemsToday;
        private Calendar calendar = Calendar.getInstance();
        private static boolean isCheckedIn = false, mRunning = false;
        private Button btnCheck;
        private TextView labelLastEntry,labelWelcome;
        private GetTime networkTime;

        public StartFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static StartFragment newInstance(int sectionNumber) {
            StartFragment fragment = new StartFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            fragment.setArguments(args);

            return fragment;
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_schedule, container, false);
            //  title.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());

            networkTime = new GetTime();

            startFragment = this;
            btnCheck = (Button) rootView.findViewById(R.id.btnCheck);
            labelWelcome = (TextView) rootView.findViewById(R.id.label_welcome);

            btnCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkIn();
                }
            });
            labelWelcome.setText(String.format(rootView.getContext().getString(R.string.welcome_format),prefs.getString("user_name", rootView.getContext().getString(R.string.pref_default_display_name))));

            ListView schedule = (ListView) rootView.findViewById(R.id.listviewSchedule);
            ListView scheduleToday = (ListView) rootView.findViewById(R.id.listviewToday);

            labelLastEntry = (TextView) rootView.findViewById(R.id.labelLastEntry);//get last ?

            //Week*
            listItems = new ArrayList<Schedule>();//create new arraylist for week schedule
            weekAdapter = new ScheduleAdapter(this.getContext(), listItems);//Create adapter
            schedule.setAdapter(weekAdapter);//Set adapter

            //Today*
            listItemsToday = new ArrayList<Schedule>(); //create new arraylist for todays schedule
            dayAdapter = new ScheduleAdapter(this.getContext(), listItemsToday); //create adapter
            scheduleToday.setAdapter(dayAdapter); //Set adapter
            ScheduleRefresh refresher = new ScheduleRefresh();
            refresher.run();

            return rootView;
        }

        /**
         * Gets the weekly schedule using a backgroundworker.
         */
        private void getSchedule() {
            System.out.println("Getting next 7 days!");
            listItems.clear();
            String today = ToolsForActivities.getDateFormatter().format(calendar.getTime());
            Calendar cal = (Calendar) calendar.clone();

            cal.add(Calendar.DAY_OF_MONTH, 7);
            String nDays = ToolsForActivities.getDateFormatter().format(cal.getTime());
            System.out.println("Days =  " + today + " till " + nDays);
            ScheduleTask scheduleTask = new ScheduleTask(today, nDays, false);
            scheduleTask.setScheduleActivity(startFragment);
            scheduleTask.execute();
        }


        /**
         * Adds week schedule to the correct adapter.
         *
         * @param schedule
         */
        public void addWeek(Schedule schedule) {
            weekAdapter.add(schedule);
        }

        /**
         * Adds todays schedule to the correct adapter
         *
         * @param schedule - Schedule to add.
         */
        public void addDay(Schedule schedule) {
            dayAdapter.add(schedule);
        }


        /**
         * TODO: Move to its own backgroundworker .!!!
         * Processing of check in and check out.
         */
        private void checkIn() {
            CheckIOTask checkIOTask = new CheckIOTask("check-in");
            //set activity here
            checkIOTask.execute();

            //This should be done in another thread
            System.out.println("Check in/out processing!");
            try {
                String entry = ToolsForActivities.getDateTimeFormatter().format(networkTime.getDate());

                if (!isCheckedIn) {
                    ToolsForActivities.toast(App.getInstance(), "Entry registered at " + entry);
                    labelLastEntry.setText("Last entry: " + "Check in " + entry);
                    btnCheck.setText("Check out");
                    btnCheck.setBackgroundResource(R.drawable.round_button_red);
                    isCheckedIn = true;
                } else {
                    ToolsForActivities.toast(App.getInstance(), "Entry registered at " + entry);
                    labelLastEntry.setText("Last entry: " + "Check out " + entry);
                    btnCheck.setText("Check in");
                    btnCheck.setBackgroundResource(R.drawable.round_button);
                    isCheckedIn = false;

                }
            } catch (NullPointerException npoint) {
                npoint.printStackTrace();
            }
        }

        @Override
        public void onPause() {
            super.onPause();  // Always call the superclass method first
            if (mRunning == true) {
                mRunning = false;
            }
        }

        @Override
        public void onResume() {
            super.onResume();  // Always call the superclass method first

            if (mRunning == false) {
                mRunning = true;
            }
        }

        class ScheduleRefresh {
            private final ScheduledExecutorService scheduler =
                    Executors.newScheduledThreadPool(1);

            public void run() {

                final Runnable refresher = new Runnable() {
                    public void run() {
                        System.out.println("Running schedule refresh!");
                        getSchedule();
                    }
                };
                final ScheduledFuture<?> refreshHandle =
                        scheduler.scheduleAtFixedRate(refresher, 0, 10, TimeUnit.SECONDS);

                //Some code to stop the refresher, dirty but works
                final Runnable stopper = new Runnable() {
                    public void run() {
                      //  System.out.println("Running refresh stopper!");
                        if(!mRunning)
                        {
                            System.out.println("Stopping Refresher!");
                            refreshHandle.cancel(true);
                            throw new RuntimeException ("Stopping stopper");
                        }
                    }
                };
                final ScheduledFuture<?> stopperHandle =
                        scheduler.scheduleAtFixedRate(stopper, 2, 2, TimeUnit.SECONDS);
            }
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class CalendarFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";


        public CalendarFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static CalendarFragment newInstance(int sectionNumber) {
            CalendarFragment fragment = new CalendarFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);

            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
         CalendarView  calendar = (CalendarView) rootView.findViewById(R.id.calendarView);

            calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView view, int year,
                                                int month, int dayOfMonth) {
                    // TODO Auto-generated method stub
                    ToolsForActivities.toast(
                            rootView.getContext(),
                            "Date is\n\n" + dayOfMonth + " / " + (month+1)
                                    + " / " + year) ;      }
            });            return rootView;
        }


    }










    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return StartFragment.newInstance(position);
                case 1:
                    return CalendarFragment.newInstance(position);

                default:
                    return StartFragment.newInstance(position);
            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Today";
                case 1:
                    return "Schedule";
                case 2:
                    return "What?";
            }
            return null;
        }
    }


}
