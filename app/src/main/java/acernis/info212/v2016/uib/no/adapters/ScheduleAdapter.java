package acernis.info212.v2016.uib.no.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import acernis.info212.v2016.uib.no.beans.Schedule;
import acernis.info212.v2016.uib.no.task.R;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

/**
 * Created by QRT on 17.11.2016.
 */

/**
 * Holds the listview list, and updates with custom view.
 */
public class ScheduleAdapter extends ArrayAdapter<Schedule> {
    public ScheduleAdapter(Context context, ArrayList<Schedule> users) {
        super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Schedule schedule = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_schedule, parent, false);
        }
        if (schedule.workDate == null) {
            return convertView;
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(convertView.getContext());

        // Lookup view for data population
        TextView ID = (TextView) convertView.findViewById(R.id.scheduleID);
        TextView date = (TextView) convertView.findViewById(R.id.scheduleDATE);
        TextView name = (TextView) convertView.findViewById(R.id.scheduleDAYNAME);
        TextView time = (TextView) convertView.findViewById(R.id.scheduleTIME);
        TextView money = (TextView) convertView.findViewById(R.id.scheduleMONEY);

        // Populate the data into the template view using the data object
        try {
            ID.setText("[" + schedule.ID + "]");
            String dayName = ToolsForActivities.getWeekNameFormatter().format(schedule.workDate);
            String dateString = ToolsForActivities.getDateFormatter().format(schedule.workDate);
            name.setText(dayName);
            date.setText(dateString);
            String sTime = ToolsForActivities.getTimeFormatter().format(schedule.startWorkTime);
            String eTime = ToolsForActivities.getTimeFormatter().format(schedule.endWorkTime);
            time.setText(sTime + " to " + eTime);

            if (prefs.getBoolean("user_show_payment", false)) {
                long hours = (schedule.endWorkTime.getTime() - schedule.startWorkTime.getTime()) / (60 * 60 * 1000);
                double pay = calculateMoneyMade(Double.parseDouble(prefs.getString("user_rate", convertView.getContext().getString(R.string.user_rate_default_value))), hours);
                money.setText(pay + " NOK");
                //System.out.println("Hours : " + hours  );
                //System.out.println("Pay : " + pay );
            }
            else
            {
                money.setText("");

            }

        } catch (NullPointerException ne) {
            ne.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        // Return the completed view to render on screen
        return convertView;
    }

    public static double calculateMoneyMade(double rate, double hours) {
        return rate * hours;


    }
}

