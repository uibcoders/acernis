package acernis.info212.v2016.uib.no.task;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import java.util.HashMap;

import acernis.info212.v2016.uib.no.activities.App;
import acernis.info212.v2016.uib.no.activities.MainActivity;
import acernis.info212.v2016.uib.no.tools.HTTPConnection;
import acernis.info212.v2016.uib.no.tools.ToolsForActivities;

/**
 * Created by stian on 16.11.2016.
 */

public class LoginTask extends AsyncTask<Object, Object, String> {
    private HashMap<String, String> connParams = new HashMap<>();
    private Activity loginActivity; //The android login activity
    private HTTPConnection connection;  //The connection to the server
    private String username, password; //Username and password

    /**
     * Initiate login with username and password
     * @param name - The username
     * @param pass - The Password
     */
    public LoginTask(String name, String pass) {
        connParams.put("m", "y");
        connParams.put("u", name);
        connParams.put("p", pass);
    }


    /**
     * Runs in background when executed from main thread
     * @param params
     * @return
     */
    @Override
    protected String doInBackground(Object[] params) {
        System.out.println("Logging IN");
        if (connParams == null) {
            System.out.println("No params set, initialize!");
            return null;
        }
        connection = HTTPConnection.getInstance();
        return ToolsForActivities.parseJsonToken(connection.getJson(connParams, null));
    }

    /**
     * Runs when background task has finished.
     * Updates main gui. Saves token for later use.
     * @param token - The token gotten from login
     */
    protected void onPostExecute(String token) {
        System.out.println(token);
        if (token != null) {
            App.setToken(token);
            //  saveToken(token);
            ToolsForActivities.toast(App.getInstance(), "Login Successful ");

            Intent intent = new Intent(getLoginActivity(), MainActivity.class);
            getLoginActivity().startActivity(intent);
            //update layout

        } else {
            ToolsForActivities.toast(App.getInstance(), "Login Failed ");

            //show error
        }


    }

    /**
     * Sets the calling login activity
     * @param loginActivity
     */
    public void setLoginActivity(Activity loginActivity) {

        this.loginActivity = loginActivity;
    }

    /**
     * Gets the login activity
     * @return
     */
    public Activity getLoginActivity() {

        return this.loginActivity;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public HashMap<String, String> getConnParams() {
        return connParams;
    }

    public void setConnParams(HashMap<String, String> connParams) {
        this.connParams = connParams;
    }

    public HTTPConnection getConnection() {
        return connection;
    }

    public void setConnection(HTTPConnection connection) {
        this.connection = connection;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
