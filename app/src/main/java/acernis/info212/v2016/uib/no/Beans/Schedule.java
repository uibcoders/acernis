package acernis.info212.v2016.uib.no.beans;


import java.sql.Date;
import java.sql.Time;

/**
 * Created by stian on 08.09.2016.
 */
public class Schedule implements Comparable<Schedule> {

    /*JSON DATA [ "ID": 40,
      "userID": 1246,
      "TSID": 0,
      "workDate": "2016-09-14",
      "startWorkTime": "09:04:00",
      "endWorkTime": "18:50:00",
      "checkIN": "",
      "checkOUT": ""
    }]

     */

    public int ID;
    private int userID;
    private int TSID;
    public Date workDate = null;
    public Time startWorkTime = null;
    public Time endWorkTime = null;
    private Time checkIN = null;
    private Time checkOUT = null;


    public Schedule() {

    }

    public Schedule(int ID, int userID, int TSID, Date workDate, Time startWorkTime, Time endWorkTime, Time checkIN, Time checkOUT) {
        setID(ID);
        setUserID(userID);
        setTSID(TSID);
        setWorkDate(workDate);
        setStartWorkTime(startWorkTime);
        setEndWorkTime(endWorkTime);
        setCheckIN(checkIN);
        setCheckOUT(checkOUT);
    }


    @Override
    public String toString() {
        return getWorkDate() + " " + getStartWorkTime() + " - " + getEndWorkTime();
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getTSID() {
        return TSID;
    }

    public void setTSID(int TSID) {
        this.TSID = TSID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Time getStartWorkTime() {
        return startWorkTime;
    }

    public void setStartWorkTime(Time startWorkTime) {
        this.startWorkTime = startWorkTime;
    }

    public Time getEndWorkTime() {
        return endWorkTime;
    }

    public void setEndWorkTime(Time endWorkTime) {
        this.endWorkTime = endWorkTime;
    }

    public Time getCheckIN() {
        return checkIN;
    }

    public void setCheckIN(Time checkIN) {
        this.checkIN = checkIN;
    }

    public Time getCheckOUT() {
        return checkOUT;
    }

    public void setCheckOUT(Time checkOUT) {
        this.checkOUT = checkOUT;
    }


    @Override
    public int compareTo(Schedule o) {
        return getWorkDate().compareTo(o.getWorkDate());
    }
}
